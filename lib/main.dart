import 'package:flutter/material.dart';
import 'package:untitled/Pages/Home.dart';
import 'package:untitled/Widgets/Pallete.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'E-Agro Design Demo',
      theme: ThemeData(
        primarySwatch: Palette.kToDark,
      ),
      home: MyHomePage(title: 'E-Agro Design Demo'),
    );
  }
}
