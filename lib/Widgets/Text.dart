import 'package:flutter/material.dart';

Widget listoftext(String content, TextStyle choose){
  return Text(
    content,
    style: choose,
  );
}

Widget listoftext2(String content, TextStyle choose, TextAlign align, num){
  return Text(
    content,
    style: choose,
    textAlign: align,
    maxLines: num,
  );
}