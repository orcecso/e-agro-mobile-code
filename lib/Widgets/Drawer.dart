import 'package:flutter/material.dart';

import 'Pallete.dart';

class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Palette2.kToDark,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: DrawerHeader(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage('images/logo.1.png',))
                ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.home),
              title: Text('HOME'),
              onTap: () => {},
            ),
            Divider(color: Colors.white,),
            ListTile(
              leading: Icon(Icons.contact_support_outlined),
              title: Text('HOW IT WORKS'),
              onTap: () => {Navigator.of(context).pop()},
            ),
            Divider(color: Colors.white,),
            ListTile(
              leading: Icon(Icons.contact_phone_sharp),
              title: Text('ABOUT US'),
              onTap: () => {Navigator.of(context).pop()},
            ),
            Divider(color: Colors.white, ),
            ListTile(
              leading: Icon(Icons.border_color),
              title: Text('KNOWLEDGE CENTER'),
              onTap: () => {Navigator.of(context).pop()},
            ),
            Divider(color: Colors.white,),
            ListTile(
              leading: Icon(Icons.agriculture),
              title: Text('FARMING 101'),
              onTap: () => {Navigator.of(context).pop()},
            ),
            Divider(color: Colors.white,),
          ],
        ),
      ),
    );
  }
}

// ignore: camel_case_types
//will sort this later