import 'package:flutter/material.dart';

Widget appibar(){
  return AppBar(
    backgroundColor: Colors.white,
    bottomOpacity: 0.0,
    elevation: 0.0,
    toolbarHeight: 100,
    title: SizedBox(
      height: 100,
      child: Image.asset('images/logo.1.png',
        fit: BoxFit.contain,
        height: 150,
        width: 150,
      ),
    ),
    actions: [
    ],
  );
}