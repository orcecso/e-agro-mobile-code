import 'package:flutter/material.dart';

import 'Orangebutton.dart';
import 'Text.dart';

Widget titleintro(){
  return Padding(
    padding: const EdgeInsets.only(left: 15.0),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: 100,
        ),
        listoftext('WELCOME TO E-AGRO ECOSYSTEM', TextStyle(color: Colors.white,fontWeight: FontWeight.bold, fontSize: 20)),
        Container(height: 30),
        listoftext('Improving', TextStyle(fontFamily: 'Handlee', color: Colors.white, fontSize: 40, fontWeight: FontWeight.bold)),
        Container(height: 5),
        listoftext('The way', TextStyle(fontFamily: 'Handlee', color: Colors.white, fontSize: 40, fontWeight: FontWeight.bold)),
        Container(height: 5),
        listoftext('Farmers Live', TextStyle(fontFamily: 'Handlee', color: Colors.white, fontSize: 40, fontWeight: FontWeight.bold)),
        Container(height: 5),
        listoftext('CONNECT • SUPPORT • LEARN', TextStyle(color: Colors.white,fontWeight: FontWeight.bold)),
        Container(height: 30),
        roundedRectButton("Learn More ...", Gradient1)
      ],
    ),
  );
}