import 'package:flutter/material.dart';
import 'package:untitled/Widgets/Text.dart';

Widget Callcontainer(width,image, logo,title,content){
  return Container(
    height: 550,
    width: width,
    decoration: ShapeDecoration(
      color: Colors.white,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0)),
    ),
    child: Column(
      children: [
        Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Image.asset(image,
              fit: BoxFit.fill,
            ),
            Container(
              color: Colors.white,
              height: 100,
              width: 100,
              child: ClipOval(
                child: Container(
                  color: Color(0xFF28a745),
                  child: Image.asset(
                    logo,
                    height: 100,
                    width: 100,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        listoftext2(title, TextStyle(color: Color(0xFF28a745), fontSize: 36, fontWeight: FontWeight.bold), TextAlign.center, 1),
        SizedBox(height: 10),
        listoftext2(content, TextStyle(color: Color(0xff6e7673),fontSize: 30), TextAlign.center, 3)
      ],
    ),
  );
}

Widget CallofImage(image){
  return Container(
    decoration: ShapeDecoration(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0)),
    ),
    child: Column(
      children: [
        Image.asset(image,
          fit: BoxFit.fill,
        ),
      ],
    ),
  );
}

Widget Callofrow(image,content){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      ClipOval(
        child: Container(
          color: Color(0xFF28a745),
          child: Image.asset(
            image,
            height: 100,
            width: 100,
            fit: BoxFit.cover,
          ),
        ),
      ),
      listoftext(content, TextStyle(color: Color(0xFF255946), fontSize: 30, fontWeight: FontWeight.bold)),
    ],
  );
}