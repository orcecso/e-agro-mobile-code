import 'package:flutter/material.dart';

Widget roundedRectButton(String title, List<Color> gradient, ) {
  return Builder(builder: (BuildContext mContext) {
    return Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: Stack(
        alignment: Alignment(1.0, 0.0),
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            height: 50,
            width: MediaQuery.of(mContext).size.width / 2.2,
            decoration: ShapeDecoration(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0)),
              gradient: LinearGradient(
                  colors: gradient,
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight),
            ),
            child: Text(title,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.bold)),
            padding: EdgeInsets.only(top: 16, bottom: 16),
          ),
        ],
      ),
    );
  });
}

const List<Color> Gradient1 = [
  Color(0xFFEFB443),
  Color(0xFFEFB443),
];

const List<Color> Gradient2 = [
  Color(0xFF49A760),
  Color(0xFF49A760),

];
