import 'package:flutter/material.dart';
import 'package:untitled/Widgets/Appbar.dart';
import 'package:untitled/Widgets/Callcontainer.dart';
import 'package:untitled/Widgets/Drawer.dart';
import 'package:untitled/Widgets/List.dart';
import 'package:untitled/Widgets/Orangebutton.dart';
import 'package:untitled/Widgets/Text.dart';
import 'package:untitled/Widgets/TitleIntro.dart';
import 'dart:math' as math;

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        endDrawer: NavDrawer(),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(100.0),
          child: appibar(),
        ),
        body: SingleChildScrollView(
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.spaceAround,
            // crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Stack(
                children: [
                  Container(
                    height: 500,
                    child: Image.asset('images/background.jpg',
                      fit: BoxFit.none,
                      width: double.infinity,
                      color: Color.fromRGBO(0, 100, 0, 0.5),
                        colorBlendMode: BlendMode.darken
                    ),
                  ),
                  titleintro(),
                ],
              ),
              Container(
                color: Color(0xFFEFB443), height: 50
              ),
              Container(
                color: Color(0xFFEFB443),
                width: MediaQuery.of(context).size.width,
                height: 1800,
                child: Column(
                  children: [
                    Callcontainer(MediaQuery.of(context).size.width / 1.2, 'images/img2.png', 'images/agriculture.png', "POVERTY REDUCTION", "Empowering farmers by giving them the means to get out of poverty."),
                    SizedBox(height: 40),
                    Callcontainer(MediaQuery.of(context).size.width / 1.2, 'images/img1.png', 'images/cereal.png', "FOOD SUSTAINABILITY", "Sustainable food supply via increased farm output at lower cost."),
                    SizedBox(height: 40),
                    Callcontainer(MediaQuery.of(context).size.width / 1.2, 'images/img1.jpg', 'images/eco-friendly.png', "INCLUSIVE GROWTH", "Economic growth for 30,850 farmers of Bayambang."),
                    SizedBox(height: 40),
                  ],
                ),
              ),
              SizedBox(height: 40),
              CallofImage('images/truck.jpg'),
              SizedBox(height: 40),
              CallofImage('images/wheat.jpg'),
              SizedBox(height: 40),
              listoftext2("WHAT IS E-AGRO ECOSYSTEM?", TextStyle(color: Color(0xFF949494), fontSize: 30, fontWeight: FontWeight.bold), TextAlign.left, 2),
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: listoftext2("Better AGRICULTURE for Better FUTURE", TextStyle(color: Color(0xFF255946), fontSize: 60, fontWeight: FontWeight.bold), TextAlign.start, 3),
              ),
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: listoftext2('The E-Agro Ecosystem, also known as "Farmer Total Assistance Platform", is a one - stop - shop of support services for farmers to prioritize their needs and unlimited opportunities for lenders and investors - all at their fingertips.', TextStyle(color: Color(0xFF6E7673), fontSize: 30, fontWeight: FontWeight.bold), TextAlign.start, 10),
              ),
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  color: Color(0xff949494),
                  width: MediaQuery.of(context).size.width / 1.2,
                  height: 2,
                ),
              ),
              SizedBox(height: 20),
              Callofrow("images/farmer.png","Farmers"),
              SizedBox(height: 5),
              Callofrow("images/businessman.png", "Investors"),
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  color: Color(0xff949494),
                  width: MediaQuery.of(context).size.width / 1.2,
                  height: 2,
                ),
              ),
              SizedBox(height: 20),
              roundedRectButton("Get Involved", Gradient1),
              roundedRectButton("Send a Message", Gradient2)
            ],
          ),
        )
      ),
    );
  }
}
